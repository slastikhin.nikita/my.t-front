FROM nginx
COPY www /usr/share/nginx/html/
RUN rm /etc/nginx/conf.d/default.conf
COPY ngx-cros.conf /etc/nginx/conf.d/
