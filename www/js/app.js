var app4 = new Vue({
    el: '#app-4',
    data: {
        machines: [],
        users: [],
        userMachineDate: [],
        div1show: true,
        div2show: false,
        div3show: false,
        logData: [],
        yearsList: [],
//        monthList: ['January','February','March','April','May','June','July','August','September','October','November','December']
        monthList: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],

        selMachID: 1,
        selUserID: 1,
		selMonth: -1,
		selYear: -1,
		baseUrl: 'http://k.ioi.pp.ua:8000'
    },
    watch: {
        selYear: function (val, oldVal) {
            console.log('новое значение: %s, старое значение: %s', val, oldVal)
        },

        },
    methods: {
        readResult: function() {
            var dataURL = this.baseUrl + '/api/1.0/get_machines';
            axios
                .get(dataURL)
                .then(response => (this.machines = response.data));

            dataURL = this.baseUrl + '/api/1.0/get_users';
            axios
                .get(dataURL)
                .then(response => (this.users = response.data));

            dataURL = this.baseUrl + '/api/1.0/get_years';
            axios
                .get(dataURL)
                .then(response => (this.yearsList = response.data));
        },
        loadLog: function(id){
            var dataURL = this.baseUrl + '/api/1.0/get_log?mach_id=' + id;
            axios
                .get(dataURL)
                .then(response => (this.logData = response.data));
            this.selMachID = id;
        },
        loadUserMachineData: function(user,month,year){
            var dataURL =   this.baseUrl + '/api/1.0/get_user_hours?' +
                            '&user_id=' + user +
                            '&year=' + year +
                            '&month=' + month;
            axios
                .get(dataURL)
                .then(response => (this.userMachineDate = response.data));
            console.log(this.userMachineDate)
        },
        showDiv: function(id) {
            switch(id) {
                case 1:
                    this.div1show = true;
                    this.div2show = false;
                    this.div3show = false;
                    break;
                case 2:
                    this.div1show = false;
                    this.div2show = true;
                    this.div3show = false;
                    break;
                case 3:
                    this.div1show = false;
                    this.div2show = false;
                    this.div3show = true;
                    this.loadLog(1);
                    break;
            }
        },
    },
    filters: {

    },
    watch: {
        selMonth: function (val, oldVal) {
            this.loadUserMachineData(this.selUserID,this.selMonth+1,this.selYear);
        },
        selYear: function (val, oldVal) {
            this.loadUserMachineData(this.selUserID,this.selMonth+1,this.selYear);
        }
    },
    mounted() {
        this.readResult();
    }
})







var app = new Vue({
    el: "#app",
    data: {
        chart: null,
        city: "",
        dates: [],
        temps: [],
        loading: false,
        errored: false
    },
    methods: {
        getData: function() {
            this.loading = true;

            if (this.chart != null) {
                this.chart.destroy();
            }

            axios
                .get("https://api.openweathermap.org/data/2.5/forecast", {
                    params: {
                        q: this.city,
                        units: "imperial",
                        appid: "fd3150a661c1ddc90d3aefdec0400de4"
                    }
                })
                .then(response => {
                    this.dates = response.data.list.map(list => {
                        return list.dt_txt;
                    });

                    this.temps = response.data.list.map(list => {
                        return list.main.temp;
                    });

                    var ctx = document.getElementById("myChart");
                    this.chart = new Chart(ctx, {
                        type: "line",
                        data: {
                            labels: this.dates,
                            datasets: [
                                {
                                    label: "Avg. Temp",
                                    backgroundColor: "rgba(54, 162, 235, 0.5)",
                                    borderColor: "rgb(54, 162, 235)",
                                    fill: false,
                                    data: this.temps
                                }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                text: "Temperature with Chart.js"
                            },
                            tooltips: {
                                callbacks: {
                                    label: function(tooltipItem, data) {
                                        var label =
                                            data.datasets[tooltipItem.datasetIndex].label || "";

                                        if (label) {
                                            label += ": ";
                                        }

                                        label += Math.floor(tooltipItem.yLabel);
                                        return label + "°F";
                                    }
                                }
                            },
                            scales: {
                                xAxes: [
                                    {
                                        type: "time",
                                        time: {
                                            unit: "hour",
                                            displayFormats: {
                                                hour: "M/DD @ hA"
                                            },
                                            tooltipFormat: "MMM. DD @ hA"
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: "Date/Time"
                                        }
                                    }
                                ],
                                yAxes: [
                                    {
                                        scaleLabel: {
                                            display: true,
                                            labelString: "Temperature (°F)"
                                        },
                                        ticks: {
                                            callback: function(value, index, values) {
                                                return value + "°F";
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    });
                })
                .catch(error => {
                    console.log(error);
                    this.errored = true;
                })
                .finally(() => (this.loading = false));
        }
    }
});